<?php

define('D_S', DIRECTORY_SEPARATOR);

require dirname(__FILE__) . D_S . 'lib' . D_S . 'App.php';

(new App())->run();