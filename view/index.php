<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="assets/style.css">
</head>
<body>
<h1><?= $title ?></h1>

<?php if ($countries): ?>
    <ul>
        <?php foreach ($countries as $country): ?>
            <li><span class="tooltip"><?= $country['name'] ?><span
                            class="tooltiptext"><?= $country['descr'] ?></span></span></li>
            <?php if ($country['cities']): ?>
                <ul>
                    <?php foreach ($country['cities'] as $city): ?>
                        <li><span class="tooltip"><?= $city['name'] ?><span
                                        class="tooltiptext"><?= $city['descr'] ?></span></span></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <?php if ($country['regions']): ?>
                <ul>
                    <?php foreach ($country['regions'] as $region): ?>
                        <li><span class="tooltip"><?= $region['name'] ?><span
                                        class="tooltiptext"><?= $region['descr'] ?></span></span></li>
                        <?php if ($region['cities']): ?>
                            <ul>
                                <?php foreach ($region['cities'] as $city): ?>
                                    <li><span class="tooltip"><?= $city['name'] ?><span
                                                    class="tooltiptext"><?= $city['descr'] ?></span></span></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

        <?php endforeach; ?>
    </ul>
<?php else: ?>
    No countries
<?php endif; ?>

</body>
</html>