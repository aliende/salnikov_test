<?php

class Renderer
{
    public static function render($template, $vars = [])
    {
        if (file_exists(dirname(__FILE__, 2) . D_S . 'view' . D_S . $template . '.php')) {
            ob_start();
            extract($vars);
            require dirname(__FILE__, 2) . D_S . 'view' . D_S . $template . '.php';
            echo ob_get_clean();
        }
    }
}