<?php

class Database
{
    final private function __construct()
    {
        /* ... @return Database */
    }

    final private function __clone()
    {
        /* ... @return Database */
    }

    final private function __wakeup()
    {
        /* ... @return Database */
    }

    private static $instance = null;

    public static function getInstance()
    {
        if (self::$instance === null) {
            $params = parse_ini_file(dirname(__DIR__, 1) . D_S . 'config.ini', true)['database'];
            try {
                self::$instance = new PDO(
                    'mysql:host=' . $params['db_host'] .
                    ';dbname=' . $params['db_name'],
                    $params['db_user'],
                    $params['db_pass']);
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                echo 'Error!' . $e->getMessage();
                die;
            }
        }

        return self::$instance;
    }

}