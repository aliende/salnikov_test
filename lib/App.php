<?php

require 'Renderer.php';
require 'Database.php';

class App
{
    private $allowed_langs = [
        'rus',
        'eng',
        'ger',
    ];

    private $user_lang = 'rus';

    private $db = null;

    public function __construct()
    {
        if (isset($_GET['user_lang']) && in_array($_GET['user_lang'], $this->allowed_langs)) {
            $this->user_lang = $_GET['user_lang'];
        } else {
            $this->user_lang = 'rus';
        }
        $this->db = Database::getInstance();
    }

    public function getUserLang()
    {
        return $this->user_lang;
    }

    private function getTitle()
    {
        if ($this->user_lang == 'rus') return 'Страны';
        elseif ($this->user_lang == 'eng') return 'Countries';
        elseif ($this->user_lang == 'ger') return 'Zustände';
    }

    private function getCities($country_id, $region_id)
    {
        $sql = "SELECT id,c_name_$this->user_lang as name,c_descr_$this->user_lang as descr from city WHERE c_region_id = :region_id AND c_country_id = :country_id";
        $pdo = $this->db->prepare($sql);
        $pdo->execute([':region_id' => $region_id, ':country_id' => $country_id]);
        return $pdo->fetchAll();
    }

    private function getRegions($country_id)
    {
        $sql = "SELECT id,r_name_$this->user_lang as name,r_descr_$this->user_lang as descr from region WHERE r_country_id = :r_country_id";
        $pdo = $this->db->prepare($sql);
        $pdo->execute([':r_country_id' => $country_id]);
        $regions = $pdo->fetchAll();
        foreach ($regions as &$region) {
            $region['cities'] = $this->getCities($country_id, $region['id']);
        }
        return $regions;
    }

    private function getCountries($region_id = 1)
    {
        $sql = "SELECT id,c_name_$this->user_lang as name,c_descr_$this->user_lang as descr from country WHERE glob_region_id = :glob_region_id";
        $pdo = $this->db->prepare($sql);
        $pdo->execute([':glob_region_id' => $region_id]);
        $countries = $pdo->fetchAll();
        foreach ($countries as &$country) {
            $country['cities'] = $this->getCities($country['id'], 0);
            $country['regions'] = $this->getRegions($country['id']);
        }
        return $countries;
    }

    public function run()
    {
        $countries = $this->getCountries();
        Renderer::render('index', [
            'title' => $this->getTitle(),
            'countries' => $countries
        ]);
    }
}